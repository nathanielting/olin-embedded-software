/***************************************************************************//**
 * @file    iec60335_class_b_critical_data.c
 * @brief   IEC60335 Class B c based secure data storage test
 *              POST and BIST secure data storage test for all compiler
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>(C) Copyright 2010 Energy Micro AS, http://www.energymicro.com</b>
 *******************************************************************************
 *
 * This source code is the property of Energy Micro AS. The source and compiled
 * code may only be used on Energy Micro "EFM32" microcontrollers.
 *
 * This copyright notice may not be removed from the source code nor changed.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Energy Micro AS has no
 * obligation to support this Software. Energy Micro AS is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Energy Micro AS will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/
/*! @addtogroup IEC60335_CRITICAL_DATA
 * @{
 */

#include "iec60335_class_b_critical_data.h"

/*!
 * @brief   all functions and macros are defined in the header file
 */

/*!
 * @}
 */

/************************************** EOF *********************************/
